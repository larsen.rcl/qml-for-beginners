// Copyright (C) 2021 The Qt Company Ltd.
// SPDX-License-Identifier: LicenseRef-Qt-Commercial OR GPL-3.0-only

import QtQuick 6.2
import Assignment4

Window {
    id: root
    width: Constants.width
    height: Constants.height

    visible: true
    title: "Assignment4"

    property int numpadSpacing: 5
    property int textHeight: 50
    property string clickedNumber: " "

    Item {
        anchors.centerIn: parent
        Column
        {
            Text {
                anchors.horizontalCenter: parent.horizontalCenter
                id: outputNumber
                text: root.clickedNumber
                font.pixelSize: root.textHeight
            }

            anchors.centerIn: parent
            spacing: root.numpadSpacing
            Row
            {
                spacing: root.numpadSpacing
                MyButton {
                    buttonText: "7"
                    textPixelHeight: root.textHeight
                    onClicked: function(value) {
                        root.clickedNumber = value
                    }
                }

                MyButton {
                    buttonText: "8"
                    textPixelHeight: root.textHeight
                    onClicked: function(value) {
                        root.clickedNumber = value
                    }
                }

                MyButton {
                    buttonText: "9"
                    textPixelHeight: root.textHeight
                    onClicked: function(value) {
                        root.clickedNumber = value
                    }
                }
            }

            Row
            {
                spacing: root.numpadSpacing
                MyButton {
                    buttonText: "4"
                    textPixelHeight: root.textHeight
                    onClicked: function(value) {
                        root.clickedNumber = value
                    }
                }

                MyButton {
                    buttonText: "5"
                    textPixelHeight: root.textHeight
                    onClicked: function(value) {
                        root.clickedNumber = value
                    }
                }

                MyButton {
                    buttonText: "6"
                    textPixelHeight: root.textHeight
                    onClicked: function(value) {
                        root.clickedNumber = value
                    }
                }
            }

            Row
            {
                spacing: root.numpadSpacing
                MyButton {
                    buttonText: "1"
                    textPixelHeight: root.textHeight
                    onClicked: function(value) {
                        root.clickedNumber = value
                    }
                }

                MyButton {
                    buttonText: "2"
                    textPixelHeight: root.textHeight
                    onClicked: function(value) {
                        root.clickedNumber = value
                    }
                }

                MyButton {
                    buttonText: "3"
                    textPixelHeight: root.textHeight
                    onClicked: function(value) {
                        root.clickedNumber = value
                    }
                }
            }

            Row
            {
                anchors.horizontalCenter: parent.horizontalCenter
                spacing: root.numpadSpacing
                MyButton {
                    buttonText: "0"
                    textPixelHeight: root.textHeight
                    onClicked: function(value) {
                        root.clickedNumber = value
                    }
                }
            }
        }
    }
}

