import QtQuick 6.2

Item {
    id: root
    property int buttonWidth: 100
    property int buttonHeight: 100
    property color baseColor: "grey"
    property color hoverColor: "lightgrey"
    property string buttonText: ""
    property int textPixelHeight: 12
    property string clickedValue: " "
    signal clicked(value: string)

    width: buttonWidth
    height: buttonHeight

    Rectangle {
        id: buttonRectangle
        anchors.fill: root
        color: root.baseColor

        Text {
            id: buttonText
            anchors.centerIn: buttonRectangle
            text: root.buttonText
            font.pixelSize: root.textPixelHeight
        }

        MouseArea {
            anchors.fill: buttonRectangle
            hoverEnabled: true

            onEntered: buttonRectangle.color = hoverColor
            onExited: buttonRectangle.color = baseColor
            onClicked: root.clicked(root.buttonText)
        }
    }
}
