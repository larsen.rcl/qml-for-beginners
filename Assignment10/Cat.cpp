#include "Cat.h"


Cat::Cat(QObject *parent)
{

}

void Cat::call()
{
    emit meow();
}

QString Cat::name()
{
    return m_Name;
}

void Cat::setName(QString value)
{
    m_Name = value;
    emit nameChanged();
}
