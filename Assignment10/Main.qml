import QtQuick
import QtQuick.Controls
import com.company.cat

Window {
    width: 640
    height: 480
    visible: true
    title: qsTr("Assignment 10")

    Cat {
        id: myCat

        onMeow: meowPopup.open()
        onNameChanged: namePopup.open()
    }

    Column {
        anchors.centerIn: parent
        spacing: 10
        width: 200
        Row {
            Label {
                text: "Name"
                height: 30
                width: 60
                horizontalAlignment: Label.AlignLeft
                verticalAlignment: Label.AlignVCenter
            }
            TextField {
                id: nameInput
                height: 30
                width: 140
            }
        }
        Button {
            anchors.horizontalCenter: parent.horizontalCenter
            id: changeButton
            height: 30
            width: 100
            text: "Change"
            onClicked: myCat.name = nameInput.text
        }
        Button {
            anchors.horizontalCenter: parent.horizontalCenter
            id: callButton
            height: 30
            width: 100
            text: "Call"
            onClicked: myCat.call()
        }
    }
    Popup {
        id: meowPopup
        anchors.centerIn: parent
        height: 30
        width: 200
        focus: true
        closePolicy: Popup.CloseOnEscape | Popup.CloseOnPressOutside
        Label {
            anchors.centerIn: parent
            text: "MEOW"
        }
    }
    Popup {
        id: namePopup
        anchors.centerIn: parent
        height: 30
        width: 200
        focus: true
        closePolicy: Popup.CloseOnEscape | Popup.CloseOnPressOutside
        Label {
            anchors.centerIn: parent
            text: "Name: " + myCat.name
        }
    }
}
