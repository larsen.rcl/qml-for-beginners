#ifndef CAT_H
#define CAT_H

#include <QObject>

class Cat : public QObject
{
    Q_OBJECT
    Q_PROPERTY(QString name READ name WRITE setName NOTIFY nameChanged FINAL)
public:
    explicit Cat(QObject* parent = nullptr);

signals:
    void meow();
    void nameChanged();

public slots:
    void call();

private:
    QString name();
    void setName(QString value);

    QString m_Name;
};

#endif // CAT_H
