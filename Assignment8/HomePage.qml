import QtQuick
import QtQuick.Controls

Page {
    width: 600
    height: 400

    title: "Home"

    Label {
        color: "#0268ce"
        text: qsTr("You are on the home page.")
        font.pointSize: 15
        font.bold: true
        anchors.centerIn: parent
    }
}

