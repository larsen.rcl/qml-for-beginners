import QtQuick
import QtQuick.Controls

Page {
    width: 600
    height: 400

    title: "Page 2"

    Label {
        color: "#0268ce"
        text: qsTr("You are on Page 2.")
        font.pointSize: 15
        font.bold: true
        anchors.centerIn: parent
    }
}
