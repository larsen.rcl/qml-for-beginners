

/*
This is a UI file (.ui.qml) that is intended to be edited in Qt Design Studio only.
It is supposed to be strictly declarative and only uses a subset of QML. If you edit
this file manually, you might introduce QML code that is not supported by Qt Design Studio.
Check out https://doc.qt.io/qtcreator/creator-quick-ui-forms.html for details on .ui.qml files.
*/
import QtQuick 6.2
import QtQuick.Controls 6.2
import Assignment2

Rectangle {
    id: rectangle
    width: Constants.width
    height: Constants.height

    Grid {
        id: grid
        anchors.fill: parent

        Rectangle {
            id: rectangle2
            width: 200
            height: 200
            visible: true
            color: rectangle2Taphandler.pressed ? "red" : "blue"

            TapHandler {
                id: rectangle2Taphandler
            }
        }
        Rectangle {
            id: rectangle3
            width: 200
            height: 200
            opacity: 0
            visible: true
            color: "#ffffff"
        }

        Rectangle {
            id: rectangle1
            x: 200
            width: 200
            height: 200
            visible: true
            color: rectangle1Taphandler.pressed ? "red" : "blue"

            TapHandler {
                id: rectangle1Taphandler
            }
        }
    }
}
