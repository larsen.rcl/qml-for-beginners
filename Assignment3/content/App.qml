// Copyright (C) 2021 The Qt Company Ltd.
// SPDX-License-Identifier: LicenseRef-Qt-Commercial OR GPL-3.0-only

import QtQuick 6.2
import Assignment3

Window {
    width: Constants.width
    height: Constants.height

    visible: true
    title: "Assignment3"

    Image {
        id: qtImage
        source: "https://upload.wikimedia.org/wikipedia/commons/thumb/6/62/Qt_Group_logo_black_2022.svg/633px-Qt_Group_logo_black_2022.svg.png"
        width: 300
        height: 100
        anchors.centerIn: parent

        Rectangle {
            id: imageRectangle
            anchors.fill: parent
            opacity: 0
            color: "green"

            MouseArea {
                id: myMouseArea
                anchors.fill: parent
                hoverEnabled: true
                acceptedButtons: Qt.LeftButton

                onEntered: imageRectangle.opacity = 0.5
                onExited: imageRectangle.opacity = 0
                onClicked: Qt.openUrlExternally("https://www.qt.io/")
            }
        }
    }

}

