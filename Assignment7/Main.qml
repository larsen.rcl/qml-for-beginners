import QtQuick
import QtQuick.Controls

Window {
    width: 640
    height: 480
    visible: true
    title: qsTr("Assignment 7")

    property int entryColumnWidth: 200
    property int labelColumnWidth: 100
    property int verticalSpacing: 10
    property int horizontalSpacing: 10
    property int rowHeight: 25
    property int buttonHeight: 25
    property int buttonWidth: 100

    Item {
        anchors.centerIn: parent
        Row {
            anchors.centerIn: parent
            id: userEntryRow
            spacing: horizontalSpacing
            Column {
                spacing: verticalSpacing
                Label {
                    id: firstNameLabel
                    height: rowHeight
                    verticalAlignment: Label.AlignVCenter
                    text: "First Name"
                }
                Label {
                    id: lastNameLabel
                    height: rowHeight
                    verticalAlignment: Label.AlignVCenter
                    text: "Last Name"
                }
                Label {
                    id: genderLabel
                    height: rowHeight
                    verticalAlignment: Label.AlignVCenter
                    text: "Gender"
                }
                Label {
                    id: agelabel
                    height: rowHeight
                    verticalAlignment: Label.AlignVCenter
                    text: "Age"
                }
            }
            Column {
                spacing: verticalSpacing
                TextField {
                    id: firstNameEntry
                    height: rowHeight
                    width: entryColumnWidth
                }
                TextField {
                    id: lastNameEntry
                    height: rowHeight
                    width: entryColumnWidth
                }
                ComboBox {
                    id: genderBox
                    height: rowHeight
                    width: entryColumnWidth
                    model: ["Male", "Female", "Other", "Prefer Not To Disclose"]
                }
                SpinBox {
                    id: ageBox
                    height: rowHeight
                    width: entryColumnWidth
                    editable: true
                }
            }
        }

        Row {
            anchors.top: userEntryRow.bottom
            anchors.horizontalCenter: userEntryRow.horizontalCenter
            topPadding: verticalSpacing
            Button {
                id: enterButton
                height: buttonHeight
                width: buttonWidth
                text: "Click Me"
                onClicked: infoPopup.open()
            }
        }
    }

    Popup {
        id: infoPopup
        anchors.centerIn: parent
        modal: true
        focus: true
        closePolicy: Popup.CloseOnEscape | Popup.CloseOnPressOutside

        Row {
            Column {
                Label {
                    text: "First Name: "
                    horizontalAlignment: Label.AlignLeft
                }
                Label {
                    text: "Last Name: "
                    horizontalAlignment: Label.AlignLeft
                }
                Label {
                    text: "Gender: "
                    horizontalAlignment: Label.AlignLeft
                }
                Label {
                    text: "Age: "
                    horizontalAlignment: Label.AlignLeft
                }
            }
            Column {
                Label {
                    text: firstNameEntry.text
                    horizontalAlignment: Label.AlignRight
                }
                Label {
                    text: lastNameEntry.text
                    horizontalAlignment: Label.AlignRight
                }
                Label {
                    text: genderBox.currentIndex
                    horizontalAlignment: Label.AlignRight
                }
                Label {
                    text: ageBox.value
                    horizontalAlignment: Label.AlignRight
                }
            }
        }
    }
}
