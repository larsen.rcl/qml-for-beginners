import QtQuick
import QtQuick.Controls
import "Code.js" as Code

Window {
    width: 640
    height: 480
    visible: true
    title: qsTr("Assignment 9")

    Column {
        anchors.centerIn: parent
        width: 150
        height: 100
        spacing: 10
        Label {
            id: label
            height: parent.height / 2
            width: parent.width
            font.pixelSize: 40
            horizontalAlignment: Text.AlignHCenter
        }
        Button {
            id: button
            height: parent.height / 2
            width: parent.width
            text: "Generate"
            onClicked: label.text = Code.random()
        }
    }
}
