// Copyright (C) 2021 The Qt Company Ltd.
// SPDX-License-Identifier: LicenseRef-Qt-Commercial OR GPL-3.0-only

import QtQuick 6.2
import Assignment5

Window {
    width: Constants.width
    height: Constants.height

    visible: true
    title: "Assignment5"

    Rectangle {
        id: mainRectangle
        color: "red"
        width: 200
        height: 200
        x: (parent.width / 2) - (width / 2)
        y: (parent.height / 2) - (height / 2)
        focus: true

        Keys.onUpPressed: mainRectangle.y -= 10
        Keys.onDownPressed: mainRectangle.y += 10
        Keys.onLeftPressed: mainRectangle.x -= 10
        Keys.onRightPressed: mainRectangle.x += 10

        Behavior on x {
            SmoothedAnimation {
                velocity: 500
            }
        }
        Behavior on y {
            SmoothedAnimation {
                velocity: 500
            }
        }
    }
}

