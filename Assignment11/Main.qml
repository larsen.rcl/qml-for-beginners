import QtQuick
import QtQuick.Controls

Window {
    width: 640
    height: 480
    visible: true
    title: qsTr("Hello World")

    Item {
        anchors.centerIn: parent
        Genie {
            id: genie
            onGenerated: resultLabel.text = value // TODO: deprecated
        }
        Label {
            anchors.top: genie.bottom
            anchors.horizontalCenter: genie.horizontalCenter
            id: resultLabel
            text: "0"
            font.bold: true
            font.pixelSize: 25
        }
    }
}
