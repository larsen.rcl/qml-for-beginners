import QtQuick
import QtQuick.Controls

Item {
    id: root
    signal generated(int value)

    Column {
        anchors.centerIn: parent
        spacing: 10
        height: 200
        width: 300
        Label {
            anchors.horizontalCenter: parent.horizontalCenter
            font.bold: true
            font.pixelSize: 25
            text: Math.floor(rangeSlider.first.value) + " to " + Math.floor(rangeSlider.second.value)
        }
        RangeSlider {
            anchors.horizontalCenter: parent.horizontalCenter
            id: rangeSlider
            from: 1
            to: 100
            first.value: 1
            second.value: 100
        }
        Button {
            anchors.horizontalCenter: parent.horizontalCenter
            text: "Generate"
            onClicked: {
                var min = rangeSlider.first.value
                var max = rangeSlider.second.value

                var result = Math.floor(Math.random() * (max - min + 1) + min)
                generated(result)
            }
        }
    }
}
